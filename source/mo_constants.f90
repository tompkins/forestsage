MODULE mo_constants
! -----------------------------------------------
! deFORESTation ScenArio GEnerator - FOREST-SAGE v1.0
! 
! dynamical deforestation model
! by
! A. M. Tompkins - all rights reserved
!                  2011 09 27
!                  tompkins@ictp.it
! -----------------------------------------------
  IMPLICIT NONE

! iforest goes to igrass
  INTEGER, PARAMETER :: npft=17, iforest=4, igrass=14

! CLM 3.5 PFTs categories
! i forest 4 = broadleaf evergreen tropical tree

! Perturbation parameters control
  LOGICAL :: lperturb(16)=.false.
  REAL :: plev
  LOGICAL :: fperturb
  REAL :: fplev
! k paramaters define the relative importance of the single local drivers 
   REAL :: k_rivers
   REAL :: k_roads
   REAL :: k_pop
   REAL :: k_frag
! p and q parameters that define the shape o beta function (betap=betaq --> maximum at 0.5)
   REAL :: betap
   REAL :: betaq
   REAL :: beta_sf
! Exponential drop off rate of land value as function of distance from road
  REAL :: road_tau
! Exponential drop off rate of land value as function of distance from river
  REAL :: river_tau
! Exponential drop off rate as function of population density and population distance
  REAL :: pop_dist_tau
  REAL :: pop_den_tau
! Afforestation timescale in years
  REAL :: tauf
! Scenario type
  REAL :: iscenario  ! 0 = constant; 1 = pessimistic scenario; 2 = optimistic-green scenario
  
! Main switches for effects
! 0 = Switch off; 1 = Switch on
  INTEGER :: Lfore=1  ! accessibility to the forest
  INTEGER :: Lroad=1  ! vicinity to roads
  INTEGER :: Lpark=1  ! inside national parks
  INTEGER :: Lpopu=1  ! population density weighting 
  INTEGER :: Llogg=1  ! logging concessions
  INTEGER :: Laffr=1  ! Reforestation
  INTEGER :: Lrive=1  ! Vicinity to rivers

! number of deforestation "ZONES"
  INTEGER, PARAMETER :: nzone=1

! annual deforestation rate
  REAL, PARAMETER :: deforest_rate(nzone) = (/0.0023/) ! FAO Central Africa 2000-2010 0.23% 

! Deforestation risk inside Park Area should be lower than sorrounding area (Values < 1)
  REAL :: park_protection(nzone)
  REAL :: park_protection_p  ! "p" perturbed parameter
  REAL :: log_area(nzone)
  REAL :: log_area_p
  real :: k_pop_p, pop_dist_tau_p, pop_den_tau_p , k_frag_p, &
    k_roads_p, k_rivers_p, road_tau_p, river_tau_p, tauf_p, betap_p, betaq_p

! area to which the deforest rate applies (this could be a N-dimensional array of areas
  REAL, PARAMETER :: zonlat1(nzone)=(/ -4/)! -4
  REAL, PARAMETER :: zonlat2(nzone)=(/ 4/) ! 4
  REAL, PARAMETER :: zonlon1(nzone)=(/ 18/) ! 18
  REAL, PARAMETER :: zonlon2(nzone)=(/ 26/) ! 26
  INTEGER, DIMENSION(1) :: zlat1(nzone),zlat2(nzone),&
                         & zlon1(nzone),zlon2(nzone) ! used for the corner indices
  INTEGER :: nzlat(nzone),nzlon(nzone)

! The above zone in contained in the following countries:
! code can be found here: http://jeppesn.dk/countrycodes.html
  INTEGER, PARAMETER :: nctry=6
  CHARACTER (len=3), DIMENSION(nctry) :: ctry=(/'CMR','COD','GAB','CAF','COG','GNQ'/)

! dimension of box to search around pft for forest fraction (should be fixed in lat/lon, not i)
  INTEGER, PARAMETER :: dxy=3 
 
! directory for GIS data
! temporary for logging concessions the data are stored on my home 
  CHARACTER(len=*), PARAMETER :: parkdata='/home/netapp-clima/users/lcaporas/sage/'
  CHARACTER(len=*), PARAMETER :: gisdata='/home/netapp-clima-shared/OBS/GISDATA/'
  CHARACTER(len=*), PARAMETER :: ncpopufile='/home/netapp-clima/users/lcaporas/sage/software/afripop_1km.nc'

! window size for the population averaging 
  INTEGER :: dxy_pop=10

! physical constants
  REAL, PARAMETER :: kmperdeg=111.0
  REAL, PARAMETER :: pi = ATAN2(0.0, -1.0)
  REAL, PARAMETER :: pi2 = 2.0*pi
  REAL, PARAMETER :: eps = EPSILON(1.0)

  contains
! perturbation -0.5-0.5  ! Perturbation of local driver parameters
  subroutine init_perturb
    implicit none
    integer , dimension(8) :: timeval
    integer , allocatable , dimension(:) :: iss
    integer :: n
    call date_and_time(values=timeval)
    call random_seed(size=n)
    allocate(iss(n))
    iss(:) = sum(timeval)
    call random_seed(put=iss)
    deallocate(iss)
  end subroutine init_perturb

  real function perturb(rval,lp)
    implicit none
    real , intent(in) :: rval
    logical , intent(in) :: lp
    real :: pmax , rr
    if ( lp ) then
      pmax = rval*plev
      call random_number(rr)
      perturb = rval + ( rr-0.5 ) * pmax
    else
      perturb = rval
    end if
  end function perturb

! functions that decribe possible scenarios (CONST/PESSIMISTIC/OPTIMISTIC/GREEN)

  real function scenario(ix,rscheme,rval)
    integer , intent(in) :: ix
    real , intent(in) :: rval , rscheme
    integer :: ischeme
    real :: x
    ischeme = int(rscheme)
    x = real(ix)
    select case (ischeme)
      case(1)
        scenario = rval*(exp(-x/(-25.0)))  ! Pessimistic Scenario 
      case(2)
        scenario = rval*(exp(-x/25.0))   ! Optimistic Scenario
      case default
        scenario = rval
    end select
  end function scenario

! Stochastic Perturbation

  subroutine spatial_perturb(matrix)
    implicit none
    real, dimension(:,:) , intent(inout) :: matrix
    real, dimension(:,:) , allocatable :: noise
    if ( .not. fperturb ) return
    allocate(noise(size(matrix,1),size(matrix,2)))
    call random_number(noise)
    where (matrix > fplev )
      matrix = max(min(matrix + (noise-0.5) * fplev,100.0),fplev)
    end where
    deallocate(noise)
  end subroutine spatial_perturb

END MODULE mo_constants
