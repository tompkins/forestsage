PROGRAM forest_sage
! -----------------------------------------------
! deFORESTation ScenArio GEnerator - FOREST-SAGE v1.0
! 
! dynamical deforestation model
! by
! A. M. Tompkins - all rights reserved
!                  2011 09 27
!                  tompkins@ictp.it
!
! deforestation risk p=p_forest*p_road*p_parks*p_log*p_river*p_pop
!
! p_forest = f^p (1-f)^q / B(p,q)
!           Beta function of the weighted forest fraction
!
! p_road = K exp (-d/tau) where d=distance to road
!         
!
! p_park = B(1,0) - simple on/off probability condition on park protection
!          can be updated to include distance to perimeter of park.
!
! p_pop = (1-e)^(-wgt_pop/tau_p)i
!
!
! reforestation = (pft_target-pft)^(-wgt_pop/200)/tauf It takes into account 
!                 the forest cover percentage and the pop density  
!
! p_log = implemented as p_park
! 
! p_river = implemented as p_road
! -----------------------------------------------

  USE netcdf
  USE mo_constants
  USE omp_lib  

  IMPLICIT NONE
  ! clm file variables
  REAL, ALLOCATABLE :: pft(:,:,:), pft_clm(:,:,:), landmask(:,:), landmask_clm(:,:), pft_ref(:,:,:)
  REAL, ALLOCATABLE :: lat(:),lon(:), lat2d(:,:), lon2d(:,:), wgt_pop(:,:)
  REAL, ALLOCATABLE :: lat_clm(:), lon_clm(:)

  ! population variables
  REAL, ALLOCATABLE :: latp(:),lonp(:),latp2d(:,:),lonp2d(:,:),popdensity(:,:)
  REAL :: rpopdensity_FillValue
  INTEGER :: nlatp, nlonp , iyear

  ! forest-sage variables
  REAL, ALLOCATABLE :: rate(:,:), mask(:,:)
  REAL, ALLOCATABLE :: road_dist(:,:), river_dist(:,:)
  REAL, ALLOCATABLE :: p_road(:,:)
  REAL, ALLOCATABLE :: p_popu(:,:)
  REAL, ALLOCATABLE :: p_park(:,:)
  REAL, ALLOCATABLE :: p_affr(:,:)
  REAL, ALLOCATABLE :: p_log(:,:)
  REAL, ALLOCATABLE :: p_river(:,:) 
  REAL, ALLOCATABLE :: p_forest(:,:)
  REAL, ALLOCATABLE :: mean_forest(:,:)
  REAL :: f_forest 
  CHARACTER (len=256) :: finfile , foutfile1 , foutfile2
  CHARACTER (len=256) , PARAMETER :: pft_reference='clm3pft_reference.dat'
  
  INTEGER :: i , id,  ilat, ilon, ilat1, ilat2, ilon1, ilon2
  INTEGER :: nlat, nlon, nlat_clm, nlon_clm, istat
  INTEGER :: ncid, ncidpop, latdimid, londimid, latvarid , lonvarid , ivarid, ipftid
  INTEGER :: nxy, izone , ierr
  REAL    :: maxpft
  REAL    :: zsf, meandef, meanaff, old_forest_sum, new_forest_sum, old_grass_sum, new_grass_sum 
  
  ! scenario deforestation rate
  real :: scenario_dfrate
  character(len=256) :: arg
  
  ! polyinzone
  LOGICAL :: do_def , do_aff

 ! Parameters that can be perturbed

  namelist /parameters/ k_rivers , k_roads , k_pop , k_frag , betap , betaq , road_tau, &
          river_tau , pop_dist_tau , pop_den_tau , tauf , park_protection , log_area ,    &
          iscenario, lperturb , plev , fperturb , fplev , iyear , finfile , foutfile1 , foutfile2

 ! Setting parameter values in the file namelist.in
!*******************************************************************************
! The ratio of increased protection inside a park relative to normal forest - function of zone -
! ratio of 0.2 means deforestation 5 times less likely inside park
  park_protection=(/0.66/)  
! Logging Concessions  deforestation rate is n times higher than the sorrounding area
  log_area = (/1.5/)  
! Exponential drop off rate of land value as function of distance from towns
  pop_dist_tau=15.   ! e-folding drop with distance km
! e-folding increase with population
  pop_den_tau=20.0 ! people/km2 
! Setting the K_values: they represent the relative importance of a single driver compared to the others
   k_rivers = 3.5  
   k_roads = 3.5   
   k_pop = 3.5     
   k_frag = 3.5   
! p and q parameters for beta rate of deforestion
   betap=4  !
   betaq=betap  !
! IF betaq=betap the location of maximum is 0.5
! Exponential drop off rate of land value as function of distance from road
  road_tau=45 ! km 
! Exponential drop off rate of land value as function of distance from river
  river_tau=5.0 ! km   
! Afforestation timescale in years
  tauf = 40  
! Define Scenario 
! Scenario = 0.0 --> Constant deforestation rate
! Scenario = 1.0 --> Increase deforestation rate
! Scenario = 2.0 --> Decrease deforestation rate 
  iscenario = 0.0
!**********************************************************************************************
  i = COMMAND_ARGUMENT_COUNT()
  IF ( i /= 1 ) THEN
    PRINT *, 'Need number of years from start of the simulation'
    STOP
  END IF

  CALL GET_COMMAND_ARGUMENT(1,arg)
  OPEN(109,file=arg,status='old',action='read',form='formatted')
  READ(109,nml=parameters,iostat=ierr)
  IF ( ierr /= 0 ) THEN
    PRINT *, 'Cannot read input namelist file!'
    STOP
  END IF
  CLOSE(109)
  CALL init_perturb

  do_def = .FALSE.
  do_aff = .FALSE.

  !---------------------------------
  ! 1. files
  !---------------------------------
  !---------------------------------
  ! 1.1 open high-res clm netcdf file 
  !---------------------------------
  PRINT *, 'Opening ',TRIM(finfile)
  CALL check(NF90_OPEN(path=finfile,mode=nf90_nowrite,ncid=ncid))
  !-----------------
  ! read lat and lon
  !----------------
  CALL check(NF90_INQ_DIMID(ncid, "latitude", LatDimID))
  CALL check(NF90_INQUIRE_DIMENSION(Ncid, LatDimID, len = nlat))
  CALL check(NF90_INQ_DIMID(ncid, "longitude", LonDimID))
  CALL check(NF90_INQUIRE_DIMENSION(Ncid, LonDimID, len = nlon))
  !---------------
  ! allocate arrays
  !---------------
  ALLOCATE(lat(nlat))
  ALLOCATE(lon(nlon))
  ALLOCATE(lat2d(nlon,nlat))
  ALLOCATE(lon2d(nlon,nlat))
  ALLOCATE(pft(nlon,nlat,0:npft-1))
  ALLOCATE(pft_ref(nlon,nlat,0:npft-1))
  ALLOCATE(landmask(nlon,nlat))
  ALLOCATE(rate(nlon,nlat))
  ALLOCATE(p_affr(nlon,nlat))
  ALLOCATE(mask(nlon,nlat))
  !---------------
  ! lat and lon
  !---------------
  CALL check(NF90_INQ_VARID(ncid, "latitude", latvarid))
  CALL check(NF90_INQ_VARID(ncid, "longitude", lonvarid))
  CALL check(NF90_GET_VAR(ncid, latvarid, lat ))
  CALL check(NF90_GET_VAR(ncid, lonvarid, lon ))
  DO ilat=1,nlat
    lat2d(:,ilat)=lat(ilat)
  ENDDO
  DO ilon=1,nlon
    lon2d(ilon,:)=lon(ilon)
  ENDDO

  !----------------------------------
  ! find which points are in the zone
  !----------------------------------
  CALL find_zone(nlon,nlat,lon,lat)
  izone=1 ! TEMPORARY
  PRINT *,'lat lon bounds',zlat1,zlat2,zlon1,zlon2,nlat,nlon

  !----------------------
  ! read the PFT variable and save the maximum value of the first pft matrix as reference
  !----------------------
  WRITE(6,'(A)') ' * reading pft data'
  CALL check(NF90_INQ_VARID(ncid, "PCT_PFT", ipftid))
  CALL check(NF90_GET_VAR(ncid, ipftid, pft ))
  CALL spatial_perturb(pft(:,:,iforest))
  ! The pft reference is equal to the initial pft matrix
  ! and it will be used during the calculation of the reforestation impact 
  OPEN(file=pft_reference,unit=97,form='unformatted', &
       status='old',iostat=istat)
  maxpft=MAXVAL(pft)
  IF ( istat /= 0 ) THEN
    pft_ref(:,:,:) = 0.0
    WHERE ( pft > 0.0 )
      pft_ref = maxpft
    END WHERE
    OPEN(file=pft_reference,unit=97,form='unformatted', &
         status='new',iostat=istat)
    IF ( istat /= 0 ) THEN
      WRITE (*,*) 'Cannot write reference pft file '
      STOP
    END IF
    WRITE(97) pft_ref
    WRITE(*,*) 'Created reference file for pft'
  ELSE
    READ(97) pft_ref
    WRITE(*,*) 'read reference pft from input file'
  END IF
  CALL check(nf90_close(ncid))
  CALL check(NF90_OPEN(path=foutfile1,mode=nf90_write,ncid=ncid))
  CLOSE(97)

  ! -------------------
  ! check for land/water  
  ! -------------------
  WHERE ( SUM(pft,3) > 0.0 )
    landmask = 1.0
  ELSEWHERE
    landmask = 0.0
  END WHERE

  !----------------
  ! population file
  !----------------
  WRITE(6,'(A)') ' * reading the population dataset'
  CALL check(NF90_OPEN(path=ncpopufile,mode=nf90_nowrite,ncid=ncidpop))
  ! check dimensions to make sure input file is correct size.
  CALL check(NF90_INQ_DIMID(ncidpop, "latitude", LatDimID))
  CALL check(NF90_INQUIRE_DIMENSION(Ncidpop, LatDimID, len = nlatp))
  CALL check(NF90_INQ_DIMID(ncidpop, "longitude", LonDimID))
  CALL check(NF90_INQUIRE_DIMENSION(Ncidpop, LonDimID, len = nlonp))

  !-----------------------------
  ! Allocation population matrix
  !-----------------------------
  ALLOCATE(lonp(nlonp),latp(nlatp))
  ALLOCATE(popdensity(nlonp,nlatp))
  ALLOCATE(latp2d(nlonp,nlatp))
  ALLOCATE(lonp2d(nlonp,nlatp))

  !---------------
  ! lat and lon
  !---------------
  CALL check(NF90_GET_VAR(ncidpop, LatDimID, latp ))
  CALL check(NF90_GET_VAR(ncidpop, LonDimID, lonp ))

  DO ilat=1,nlatp
    latp2d(:,ilat)=latp(ilat)
  ENDDO  
  DO ilon=1,nlonp
    lonp2d(ilon,:)=lonp(ilon)
  ENDDO

  !----------
  ! read data
  !----------
  CALL check(NF90_INQ_VARID(ncidpop, "population", iVarId))
  CALL check(NF90_GET_VAR(ncidpop, iVarId, popdensity))
  CALL check(NF90_GET_ATT(ncidpop, iVarId, "_FillValue", rpopdensity_FillValue))
  CALL check(NF90_CLOSE(ncidpop))

  !---------------
  ! 1.1 Initialize
  !---------------
  rate=1.0
  PRINT *,'* calculating forest density'
  mask=0.0
  izone = 1
  WHERE(lat2d>zonlat1(izone).and.lat2d<zonlat2(izone).and. & 
        lon2d>zonlon1(izone).and.lon2d<zonlon2(izone))
    mask=1.0
  END WHERE

  PRINT *,'* calculating weighted population density'
  ALLOCATE(p_popu(nlon,nlat),wgt_pop(nlon,nlat))
  izone = 1 ! hard wired zone
  CALL weighted_pop(nlonp,nlatp,latp,lonp,lonp2d,latp2d,popdensity,rpopdensity_FillValue, &
                      nlon,nlat,lat,lon,wgt_pop,izone)

  pop_dist_tau_p=perturb(pop_dist_tau,lperturb(10))
  pop_den_tau_p=perturb(pop_den_tau,lperturb(11))
  k_pop_p = perturb(k_pop,lperturb(12))
  p_popu=(k_pop_p-1.0)*(1.0-EXP(-wgt_pop/pop_den_tau_p))+1.0  
  p_popu=MIN(MAX(p_popu,1.0),k_pop_p)

  !-------------------------------------------------------------------- 
  ! 2.  Calculate the local forest "density" and the fragmentation risk
  !--------------------------------------------------------------------  
  IF (Lfore==1) THEN
    k_frag_p = perturb(k_frag,lperturb(1))
    WRITE(6,'(A)')' * calculating weighted forest density impact'
    ALLOCATE(p_forest(nlon,nlat),mean_forest(nlon,nlat))
    izone = 1
    betap_p = perturb(betap,lperturb(2))
    betaq_p = betap_p
    beta_sf=0.5**(betap_p+betaq_p) ! NOTE this is only true for symmetrical beta.
    DO ilat=1,nlat
      !$OMP PARALLEL DO PRIVATE(ilat1,ilat2,ilon1,ilon2,nxy,f_forest)
      DO ilon=1,nlon
        ilat1=MAX(1,ilat-dxy)
        ilat2=MIN(nlat,ilat+dxy)
        ilon1=MAX(1,ilon-dxy)
        ilon2=MIN(nlon,ilon+dxy)
        nxy=(ilat2-ilat1+1)*(ilon2-ilon1+1)
        f_forest=0.01*(SUM(pft(ilon1:ilon2,ilat1:ilat2,iforest)) + &
             7.0*pft(ilon,ilat,iforest))/(nxy+7.0)
        p_forest(ilon,ilat) = MIN(MAX((f_forest**betap_p * (1.0-f_forest)**betaq_p)/beta_sf,0.0),1.0)
        p_forest(ilon,ilat) = (k_frag_p-1.0)*p_forest(ilon,ilat)+1.0
      ENDDO 
      !$OMP END PARALLEL DO
    ENDDO
    rate=rate*p_forest
    print*,'risk of deforestation after fragmentation',(SUM(mask(:,:)*rate(:,:)/SUM(mask(:,:))))
    DEALLOCATE(p_forest,mean_forest)
  ENDIF
  meandef = (SUM(mask(:,:)*rate(:,:)/sum(mask(:,:))))
  PRINT *, ' RISK is: ' ,meandef

  !---------------------
  ! 3. Get the GIS data!
  !---------------------
  ! 3.1 Roads
  !----------
  ! allocate distance array
  IF (Lroad==1) THEN
    road_tau_p=perturb(road_tau,lperturb(4))
    k_roads_p = perturb(k_roads,lperturb(5))
    WRITE(6,'(A,A)') CHAR(13),' * calculating roads impact'
    ALLOCATE(p_road(nlon,nlat),road_dist(nlon,nlat))
    izone=1
    road_dist(:,:) =1.e33 ! initialize to a large number
    CALL disttoobj(nlon,nlat,lat,lon,road_dist,izone,1)
    p_road=((k_roads_p-1.0)*EXP(-road_dist/road_tau_p))+1.0
    p_road=MIN(MAX(p_road,1.0),k_roads_p)  ! safety
    rate=rate*p_road
    DEALLOCATE(p_road,road_dist)
  ENDIF 

  meandef = (SUM(mask(:,:)*rate(:,:)/SUM(mask(:,:))))
  PRINT *, 'RISK of deforestation after road :' ,meandef

  !------------------------
  ! 3.2 National Parks data
  !------------------------
  IF (Lpark==1) THEN
    park_protection_p=perturb(park_protection(izone),lperturb(6))
    WRITE(6,'(A,A)') CHAR(13),' * calculating parks impact'
    ALLOCATE(p_park(nlon,nlat))
    p_park=1.0
    izone=1
    CALL get_pobj(nlon,nlat,lon,lat,p_park,izone,1,park_protection_p)
    rate=rate*p_park
   DEALLOCATE(p_park)
  ENDIF 

  meandef = (SUM(mask(:,:)*rate(:,:)/sum(mask(:,:))))
  print *, 'RISK of deforestation after park :' ,meandef

  !------------------------
  ! 3.3 Logging Concessions
  !------------------------
  IF (Llogg==1) THEN
    log_area_p=perturb(log_area(izone),lperturb(7))
    WRITE(6,'(A,A)') CHAR(13),' * calculating logging concessions impact'
    ALLOCATE(p_log(nlon,nlat))
    p_log=1.0
    izone=1
    CALL get_pobj(nlon,nlat,lon,lat,p_log,izone,2,log_area_p)
    rate=rate*p_log
    DEALLOCATE(p_log)
  ENDIF

  meandef = (SUM(mask(:,:)*rate(:,:)/SUM(mask(:,:))))
  PRINT *, 'RISK of deforestation after logging :' ,meandef

  !----------
  ! 3.4 River 
  !----------
  IF (Lrive==1) THEN
    river_tau_p = perturb(river_tau,lperturb(8))
    k_rivers_p = perturb(k_rivers,lperturb(9))
    WRITE(6,'(A,A)') CHAR(13),' * calculating River impact'
    ALLOCATE(p_river(nlon,nlat),river_dist(nlon,nlat))
    izone = 1
    river_dist(:,:) =1.e33 ! initialize to a large number
    CALL disttoobj(nlon,nlat,lat,lon,river_dist,izone,2) ! small rivers
    CALL disttoobj(nlon,nlat,lat,lon,river_dist,izone,3) ! big rivers+lakes
    p_river=((k_rivers_p-1.0)*EXP(-river_dist/river_tau_p))+1.0
    p_river=MIN(MAX(p_river,1.0),k_rivers_p) ! safety
    rate=rate*p_river
  DEALLOCATE(p_river,river_dist)
  ENDIF ! lriver
  
  meandef = (SUM(mask(:,:)*rate(:,:)/SUM(mask(:,:))))
  PRINT *, 'RISK of deforestation after river :' ,meandef

  !----------------------- 
  ! 3.5 Population density
  !-----------------------
  IF (Lpopu==1) THEN 
    rate=rate*p_popu
  ENDIF 
  meandef = (SUM(mask(:,:)*rate(:,:)/sum(mask(:,:))))
  PRINT *, 'The final RISK is:' ,meandef
 
  !------------------
  ! 3.6 Reforestation
  !------------------
  IF ( Laffr==1 ) then
    !ALLOCATE(wgt_pop(nlon,nlat))
    !izone = 1
    !CALL weighted_pop(nlonp,nlatp,latp,lonp,lonp2d,latp2d, &
    !                  popdensity,rpopdensity_FillValue, &
    !                  nlon,nlat,lat,lon,wgt_pop,izone)

    pft_ref(:,:,iforest)=pft_ref(:,:,iforest)*EXP(-wgt_pop/200.0) 
    tauf_p = perturb(tauf, lperturb(13))
    PRINT *,'tauf_p is equal to', tauf_p
    p_affr=MAX((mask(:,:)*(pft_ref(:,:,iforest)-pft(:,:,iforest))/tauf_p),0.0) 
    !DEALLOCATE(wgt_pop)
  ENDIF

  !--------------------------
  ! Mean reforestation risk
  !--------------------------
    meanaff = (SUM(mask(:,:)*p_affr(:,:)/SUM(mask(:,:))))
    PRINT *, 'RISK of reforestation :',meanaff
  !------------------------
  !-------------------------------------------------------------------------------- 
  ! 4. normalize to give the correct annual deforestation rate in the area of focus
  !--------------------------------------------------------------------------------
  DO izone=1,nzone
      mask=0.0
      WHERE(lat2d>zonlat1(izone).and.lat2d<zonlat2(izone).and. & 
            lon2d>zonlon1(izone).and.lon2d<zonlon2(izone))
        mask=1.0
      END WHERE
      ! scale factor to get correct rate in zone...
      IF (SUM(mask)<=0.0) STOP 'no forest in zone!'
      ! Reforestation rate
      meanaff = SUM(mask(:,:)*p_affr(:,:))/SUM(mask(:,:))
      ! Scale factor to normalize the deforestation 
      scenario_dfrate = scenario(iyear,iscenario,deforest_rate(izone))
      PRINT*,'macrodef_rate',deforest_rate(izone)
      PRINT*,'scenario rate',scenario_dfrate
      ! Calculate the scale factor -Passing from Deforestation Risk to Deforestation Rate-
      zsf=(scenario_dfrate*SUM(mask(:,:)*pft(:,:,iforest))+ SUM(mask(:,:)*p_affr(:,:)))&
      & /SUM(mask(:,:)*rate(:,:)*pft(:,:,iforest))  
      PRINT *, '*********** Now starting deforestation normalization ************'
      PRINT *, 'Computed mean rate deforestation :' , meandef
      PRINT *, 'Computed mean rate afforestation :' , meanaff
      PRINT *, 'Wanted mean rate deforestation  :'  , scenario_dfrate
      PRINT *, 'Correction deforestation factor is:',zsf
      !------------------------------
      !"old" forest/grass percentage
      old_forest_sum = SUM(mask(:,:)*pft(:,:,iforest))/SUM(mask(:,:))
      old_grass_sum = SUM(mask(:,:)*pft(:,:,igrass))/SUM(mask(:,:))
      !------------------------------------------------------------------------
      ! Deforestation rate normalized taking into account the reforestation
      rate=rate*zsf*mask*pft(:,:,iforest)-mask*p_affr ! rate is zero outside zone due to mask
      !------------------------------------------------------------------------
      ! Calculation of new forest/grass percentage
      pft(:,:,iforest)=pft(:,:,iforest)-rate
      pft(:,:,igrass)=pft(:,:,igrass)+rate
      new_forest_sum = SUM(mask(:,:)*pft(:,:,iforest))/SUM(mask(:,:))
      new_grass_sum = SUM(mask(:,:)*pft(:,:,igrass))/SUM(mask(:,:))
      !------------------------------
      PRINT *, 'old forest mean = ', old_forest_sum
      PRINT *, 'new forest mean = ', new_forest_sum
      PRINT *, 'Absolute difference old forest new forest', old_forest_sum-new_forest_sum 
      PRINT *, 'Computed deforestation rate (old forest - new forest)/old forest  :', (old_forest_sum-new_forest_sum)/old_forest_sum
      PRINT *, 'Difference computed-wanted', ((old_forest_sum-new_forest_sum)/old_forest_sum)-scenario_dfrate
      PRINT *, 'old grass = ', old_grass_sum
      PRINT *, 'new grass = ', new_grass_sum
      PRINT *, 'Absolute difference old grass new grass', old_grass_sum-new_grass_sum

   ENDDO

  !---------------------------------------------
  ! 6. rewrite the variable in high res PFT file
  !---------------------------------------------
  CALL check(NF90_PUT_VAR(ncid, ipftid, pft)) 
  CALL check(NF90_CLOSE(ncid))

  !-------------------------------------
  ! 7. rewrite the variable in lowres CLM file
  !-------------------------------------
  ! note high res file is closed so can reuse the ncid
  PRINT *,'writing low resolution data'

  !---------------------------------
  ! 7.1 open low-res clm netcdf file 
  !---------------------------------
  CALL check(NF90_OPEN(path=foutfile2,mode=nf90_write,ncid=ncid))

  ! read lat and lon
  CALL check(NF90_INQ_DIMID(ncid, "lat", LatDimID))
  CALL check(NF90_INQUIRE_DIMENSION(ncid, LatDimID, len = nlat_clm))
  CALL check(NF90_INQ_DIMID(ncid, "lon", LonDimID))
  CALL check(NF90_INQUIRE_DIMENSION(Ncid, LonDimID, len = nlon_clm))

  ! read the land mask
  
  ALLOCATE(lat_clm(nlat_clm))
  CALL check(NF90_INQ_VARID(ncid, "LAT", iVarId))
  CALL check(NF90_GET_VAR(ncid, iVarId, lat_clm ))
  ALLOCATE(lon_clm(nlon_clm))
  CALL check(NF90_INQ_VARID(ncid, "LON", iVarId))
  CALL check(NF90_GET_VAR(ncid, iVarId, lon_clm ))

  ALLOCATE(landmask_clm(nlon_clm,nlat_clm))
  CALL check(NF90_INQ_VARID(ncid, "LANDMASK", iVarId))
  CALL check(NF90_GET_VAR(ncid, iVarId, landmask_clm ))

  ALLOCATE(pft_clm(nlon_clm,nlat_clm,0:npft-1))
  CALL check(NF90_INQ_VARID(ncid, "PCT_PFT", iVarId))
  CALL check(NF90_GET_VAR(ncid, iVarId, pft_clm ))

  ! aggregrate the PFT top coarse scale for CLM 

END program forest_sage

!---------------------------------------------------------

SUBROUTINE check(status)
  USE netcdf
  IMPLICIT NONE
  INTEGER, INTENT(in) :: status
  IF (status /= nf90_noerr) THEN
    PRINT *,TRIM(NF90_STRERROR(status))
    STOP 'Bad NETCDF status'
  END IF
END SUBROUTINE check

!-----------------------
SUBROUTINE get_pobj(nlon,nlat,lon,lat,pobj,izone,iobj,factor)

  USE mo_constants
  USE shapelib
  USE omp_lib

  USE,INTRINSIC :: ISO_C_BINDING
  IMPLICIT NONE 
  INTEGER , INTENT(in) :: nlon , nlat , iobj
  INTEGER , INTENT(in) :: izone
  REAL , INTENT(in) :: factor
  REAL , INTENT(in) , DIMENSION(nlon) :: lon
  REAL , INTENT(in) , DIMENSION(nlat) :: lat
  REAL , INTENT(out) , DIMENSION(nlon,nlat) :: pobj
  INTEGER :: nvert , nfield , nrec , err , nshp, tshp , ishp , ninpfiles , ip , id
  REAL, ALLOCATABLE :: xx(:),yy(:)
  TYPE(shpfileobject) :: shphandle
  TYPE(shpobject) :: shpobj
  CHARACTER(len=1024) :: filename
  CHARACTER(len=4) :: stuff
  REAL(kind=c_double) :: minbound(4), maxbound(4)
  LOGICAL :: polyinzone

  IF ( iobj == 1 ) THEN
     ninpfiles = 1 
  ELSE
    ninpfiles = nctry
  END IF
  DO ip=1,ninpfiles
    IF ( iobj == 1 ) THEN
      filename = '/scratch/cgotangc/FOREST_SAGE/testPHL1/testPHL1_GISDATA/WDPA_general_PH-shapefile/general_PH-shapefile-polygons' 
      !CKG      filename = gisdata//'PROTECTED_AREAS/WDPA_INTpol2010/WDPA_INTpol2010'
      stuff = 'park'
    ELSE IF ( iobj == 2 ) THEN
      filename=parkdata//'LOGGING_CONCESSIONS/'//TRIM(ctry(ip))//'_log_concessions'
      stuff = TRIM(ctry(ip))
    ELSE
      WRITE (*,*) 'Unknown object in get_pobj: ',iobj
      STOP
    END IF
    shphandle = shpopen(TRIM(filename), 'rb')
    IF (shpfileisnull(shphandle) .OR. dbffileisnull(shphandle)) THEN
      PRINT*,'Error opening ',TRIM(filename),' for reading'
      STOP 1
    ENDIF

    ! get general information about the shapefile object
    CALL shpgetinfo(shphandle, nshp, tshp, minbound, maxbound, nfield, nrec)
  
    ! read the nshp shapes

    DO ishp = 0, nshp - 1
      WRITE(0,'(A,A,A,A,I5)',ADVANCE='no') CHAR(13),' shape file ',stuff,' no ',ishp
      ! read the i-th shape from the shapefile object and obtain a shape object
      shpobj = shpreadobject(shphandle, ishp)
      IF (shpisnull(shpobj)) THEN
        PRINT*,'Error in shpreadobect',ishp
        STOP 1
      ENDIF
      nvert=shpobj%nvertices
      ALLOCATE(xx(nvert),yy(nvert),stat=err)
      IF(err/=0) STOP 'Error in allocating memory'
      xx(:)=REAL(shpobj%padfx(:))
      yy(:)=REAL(shpobj%padfy(:))
      IF (polyinzone(xx,yy,nvert,izone)) THEN
        CALL ptinpoly(nlon,nlat,pobj,factor,lat,lon,xx,yy,nvert)
      ENDIF 
      DEALLOCATE(xx,yy,stat=err)
      IF(err/=0) STOP 'Error in releasing memory'
      ! destroy the shape object to avoid memory leaks
      ! notice that for accessing dbf attributes the shape object is not required
      CALL shpdestroyobject(shpobj)
    ENDDO
    ! close the shapefile object
    CALL shpclose(shphandle)
  ENDDO
END SUBROUTINE get_pobj

!----------------------------------------------------------------

SUBROUTINE ptinpoly(nlon,nlat,p_area,factor,lat,lon,xx,yy,nvert)
  USE mo_constants
  IMPLICIT NONE
  INTEGER , INTENT(in) :: nlon , nlat , nvert
  REAL, INTENT(IN) , DIMENSION(nlon) :: lon
  REAL, INTENT(IN) , DIMENSION(nlat) :: lat
  REAL, INTENT(out) , DIMENSION(nlon,nlat) :: p_area
  REAL, INTENT(IN)  :: factor ! risk faction with polygon
  REAL, INTENT(IN) :: xx(nvert), yy(nvert)
  INTEGER :: ivert, ilat, ilon , izone
  REAL    :: tot 
  REAL   :: theta(nvert),dtheta(nvert)

  dtheta=0.0
  DO izone=1,nzone
    !$OMP PARALLEL DO PRIVATE (theta,dtheta,tot)
    DO ilat=zlat1(izone),zlat2(izone)
      DO ilon=zlon1(izone),zlon2(izone)
        theta=ATAN2(yy-lat(ilat),xx-lon(ilon)) ! angle to point
        dtheta=CSHIFT(theta,1)-theta ! difference in angle
        WHERE(ABS(dtheta)>pi)dtheta=dtheta-SIGN(pi2,dtheta) ! large swings safety
        tot=ABS(SUM(dtheta)) ! sum the angles
        IF(tot>pi)p_area(ilon,ilat)=factor ! 2pi=in, 0=out 
      ENDDO
    ENDDO
    !$OMP END PARALLEL DO   
  ENDDO
END SUBROUTINE ptinpoly

!-----------------------

SUBROUTINE find_zone(nlon,nlat,lon,lat)
  USE mo_constants
  IMPLICIT NONE
  INTEGER , INTENT(in) :: nlon,nlat
  REAL, INTENT(IN) , DIMENSION(nlon) :: lon
  REAL, INTENT(IN) , DIMENSION(nlat) :: lat
  INTEGER :: izone
  INTEGER, DIMENSION(1) :: iloc

  DO izone=1,nzone
    iloc=MINLOC(lat,lat>zonlat1(izone))
    zlat1(izone)=iloc(1)
    iloc=MAXLOC(lat,lat<zonlat2(izone))
    zlat2(izone)=iloc(1)
    iloc=MINLOC(lon,lon>zonlon1(izone))
    zlon1(izone)=iloc(1)
    iloc=MAXLOC(lon,lon<zonlon2(izone))
    zlon2(izone)=iloc(1)
  ENDDO
END SUBROUTINE find_zone

!--------------------------

LOGICAL FUNCTION polyinzone(xx,yy,nvert,izone)
  USE mo_constants
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: nvert , izone
  REAL, INTENT(IN) :: xx(nvert), yy(nvert)
  INTEGER :: ivert
  polyinzone=.FALSE.
  DO ivert=1,nvert
    IF (xx(ivert)>=zonlon1(izone).and.xx(ivert)<=zonlon2(izone).and. &
        yy(ivert)>=zonlat1(izone).and.yy(ivert)<=zonlat2(izone)) polyinzone=.TRUE.
  ENDDO
END FUNCTION polyinzone

!---------------------------

SUBROUTINE weighted_pop(nlonp,nlatp,latp,lonp,lonp2d,latp2d,popdensity,fillv, &
                        nlon,nlat,lat,lon,wgt_pop,izone)
  USE mo_constants
  IMPLICIT NONE 
  INTEGER , INTENT(in) :: nlonp , nlatp
  INTEGER , INTENT(in) :: nlon , nlat , izone
  REAL , DIMENSION(nlatp) , INTENT(in) :: latp
  REAL , DIMENSION(nlonp) , INTENT(in) :: lonp
  REAL , INTENT(in) :: fillv
  REAL , DIMENSION(nlonp,nlatp) , INTENT(in) :: lonp2d , latp2d , popdensity
  REAL , DIMENSION(nlon) , INTENT(in) :: lon
  REAL , DIMENSION(nlat) , INTENT(in) :: lat
  REAL , DIMENSION(nlon,nlat) , INTENT(out) :: wgt_pop

  REAL, ALLOCATABLE :: dist(:,:),mask(:,:)
  REAL :: wgt
  integer :: ilat , ilon , nxy , ilon1 , ilon2 , ilat1 , ilat2
  INTEGER nearlat(nlat),nearlon(nlon)
  INTEGER, DIMENSION(1) :: iloc

  !-------------------------------------
  ! find the nearest population gridpoint
  !-------------------------------------
  DO ilat=1,nlat
    iloc=MINLOC(ABS(lat(ilat)-latp(:)))
    nearlat(ilat)=iloc(1)
  ENDDO
  DO ilon=1,nlon
    iloc=MINLOC(ABS(lon(ilon)-lonp(:)))
    nearlon(ilon)=iloc(1)
  ENDDO 

  !--------------------------------------------------
  ! calculate the weighted population in a KxK window
  !--------------------------------------------------
  
  DO ilat=zlat1(izone),zlat2(izone)
    !$OMP PARALLEL DO PRIVATE(dist,mask,wgt)
    DO ilon=zlon1(izone),zlon2(izone)
      ilat1=MAX(1,nearlat(ilat)-dxy_pop)
      ilat2=MIN(nlatp,nearlat(ilat)+dxy_pop)
      ilon1=MAX(1,nearlon(ilon)-dxy_pop)
      ilon2=MIN(nlonp,nearlon(ilon)+dxy_pop)

      ALLOCATE(dist(ilon1:ilon2,ilat1:ilat2))
      ALLOCATE(mask(ilon1:ilon2,ilat1:ilat2))

      dist(ilon1:ilon2,ilat1:ilat2)=kmperdeg*  &
        SQRT((lonp2d(ilon1:ilon2,ilat1:ilat2)-lon(ilon))**2 + &
        (latp2d(ilon1:ilon2,ilat1:ilat2)-lat(ilat))**2)

      WHERE (dist<=0.0)dist=1.0
      WHERE (dist>=200.0)dist=200.0 ! safety to avoid overflow...
      WHERE (popdensity(ilon1:ilon2,ilat1:ilat2)==fillv)
        mask=0.0
      ELSE WHERE 
        mask=1.0
      END WHERE
      wgt=MAX(SUM(mask*EXP(-dist/pop_dist_tau_p)),1e-6)
      wgt_pop(ilon,ilat)=MAX(0.0,SUM(mask*popdensity(ilon1:ilon2,ilat1:ilat2)*EXP(-dist/pop_dist_tau_p))/wgt)
      DEALLOCATE(dist,mask)
    ENDDO
    !$OMP END PARALLEL DO
  ENDDO
END SUBROUTINE weighted_pop

! Calculate distance from roads/rivers

SUBROUTINE disttoobj(nlon,nlat,lat,lon,objdist,izone,iobj)
  USE mo_constants
  USE shapelib
  USE,INTRINSIC :: ISO_C_BINDING
  IMPLICIT NONE
  INTEGER , INTENT(in) :: nlat , nlon , iobj
  REAL , DIMENSION(nlat) :: lat
  REAL , DIMENSION(nlon) :: lon
  REAL , DIMENSION(nlon,nlat) , INTENT(out) :: objdist
  INTEGER , INTENT(in) :: izone
  REAL, ALLOCATABLE :: xx(:),yy(:),dist(:)
  TYPE(shpfileobject) :: shphandle
  TYPE(shpobject) :: shpobj
  INTEGER :: nvert , err , ictry , ilat , ilon , ishp , nfield , nrec , nshp , tshp
  REAL :: distkm
  CHARACTER(len=1024) :: filename
  REAL(kind=c_double) :: minbound(4), maxbound(4)

  DO ictry=1,nctry

    IF ( iobj == 1 ) THEN
      filename=gisdata//'COUNTRIES/'//TRIM(ctry(ictry))//'/rds/'//TRIM(ctry(ictry))//'_roads'
    ELSE IF ( iobj == 2 ) THEN
      filename=gisdata//'COUNTRIES/'//TRIM(ctry(ictry))//'/wat/'//TRIM(ctry(ictry))//'_water_lines_dcw'
    ELSE IF ( iobj == 3 ) THEN
      filename=gisdata//'COUNTRIES/'//TRIM(ctry(ictry))//'/wat/'//TRIM(ctry(ictry))//'_water_areas_dcw'
    ELSE
      WRITE (*,*) 'Unknown object in disttoobj: ',iobj
      STOP
    END IF

    shphandle = shpopen(TRIM(filename), 'rb') 
    ! error check
    IF (shpfileisnull(shphandle) .OR. dbffileisnull(shphandle)) THEN
      PRINT*,'Error opening ',TRIM(filename),' for reading'
      STOP 1
    ENDIF

    ! get general information about the shapefile object
    CALL shpgetinfo(shphandle, nshp, tshp, minbound, maxbound, nfield, nrec)

    ! read the nshp shapes
    DO ishp = 0,nshp - 1

      WRITE(6,'(A,A,A,A,I5)',ADVANCE='no') &
       CHAR(13),'shape file ',TRIM(ctry(ictry)),' obj no ',ishp+1

      ! read the i-th shape from the shapefile object and obtain a shape object
      shpobj = shpreadobject(shphandle, ishp)
      IF (shpisnull(shpobj)) THEN
        PRINT*,'Error in shpreadobject',ishp
        STOP 1
      ENDIF
      nvert=shpobj%nvertices

      ALLOCATE(xx(nvert),yy(nvert),dist(nvert),stat=err)
      IF(err/=0) STOP 'Error in allocating memory'
      xx(:)=real(shpobj%padfx(:))
      yy(:)=real(shpobj%padfy(:))

      DO ilat=zlat1(izone),zlat2(izone)
        !$OMP PARALLEL DO PRIVATE(dist,distkm) 
        DO ilon=zlon1(izone),zlon2(izone)
          dist(:)=(XX(:)-lon(ilon))**2+(YY(:)-lat(ilat))**2
          distkm=SQRT(MINVAL(dist))*kmperdeg
          objdist(ilon,ilat)=MIN(objdist(ilon,ilat),distkm)
        ENDDO
        !$OMP END PARALLEL DO
      ENDDO
      DEALLOCATE(xx,yy,dist,stat=err)
      IF(err/=0) STOP 'Error in releasing memory'
      ! destroy the shape object to avoid memory leaks
      ! notice that for accessing dbf attributes the shape object is not required
      CALL shpdestroyobject(shpobj)
    ENDDO
    WRITE (6,*) ''
    ! close the shapefile object
    CALL shpclose(shphandle)
  ENDDO
END SUBROUTINE disttoobj

  RECURSIVE SUBROUTINE sortpatch(vals,svals,ird,lsub)
    IMPLICIT NONE
    REAL , DIMENSION(:) , INTENT(in) :: vals
    REAL , DIMENSION(:) , INTENT(inout) :: svals
    INTEGER , DIMENSION(:) , INTENT(inout) :: ird
    LOGICAL , OPTIONAL :: lsub
    INTEGER :: i , iswap
    REAL :: rswap
    IF ( .not. present(lsub) ) THEN
      DO i = 1 , SIZE(vals)
        ird(i) = i
        svals(i) = vals(i)
      END DO
    END IF
    DO i = 1 , SIZE(vals)-1
      IF ( svals(i) < svals(i+1) ) THEN
        rswap = svals(i+1)
        iswap = ird(i+1)
        svals(i+1) = svals(i)
        ird(i+1) = ird(i)
        svals(i) = rswap
        ird(i) = iswap
        CALL sortpatch(vals,svals,ird,.TRUE.)
      END IF
    END DO
  END SUBROUTINE sortpatch

