PROGRAM interpolate
! -----------------------------------------------
! deFORESTation ScenArio GEnerator - FOREST-SAGE v1.0
! -----------------------------------------------

  USE netcdf

  IMPLICIT NONE

  ! clm file variables
  REAL, ALLOCATABLE :: pft(:,:,:), pft_clm(:,:,:), landmask(:,:) , landmask_clm(:,:)
  REAL, ALLOCATABLE :: pft_clm_org(:,:,:)
  REAL, ALLOCATABLE :: vals_swap(:)
  integer , allocatable :: iord(:)

  CHARACTER (len=*), PARAMETER :: pft_highres='clm3pft_xxxx_005global.nc'
  CHARACTER (len=*), PARAMETER :: clm_lowres='clm3pft_xxxx_05global.nc'

  INTEGER :: ilat,ilon, ilat1,ilat2,ilon1,ilon2,ii,jj
  integer :: dimid
  INTEGER :: nlat,nlon,nlat_clm,nlon_clm,ipft,npft
  INTEGER :: ncid, ivarid
  INTEGER :: res_fac
  REAL    :: res_fac_m2
  REAL    :: oldpft
  REAL :: pxerr , totadj , adjust , totpft

  !---------------------------------
  ! 1. files
  !---------------------------------

  !---------------------------------
  ! 1.1 open high-res clm netcdf file 
  !---------------------------------
  CALL check(NF90_OPEN(path=pft_highres,mode=nf90_write,ncid=ncid))

  ! read lat and lon
  CALL check(NF90_INQ_DIMID(ncid, "latitude", dimid))
  CALL check(NF90_INQUIRE_DIMENSION(Ncid, dimid, len = nlat))
  CALL check(NF90_INQ_DIMID(ncid, "longitude", dimid))
  CALL check(NF90_INQUIRE_DIMENSION(Ncid, dimid, len = nlon))
  CALL check(NF90_INQ_DIMID(ncid, "pft", dimid))
  CALL check(NF90_INQUIRE_DIMENSION(Ncid, dimid, len = npft))

  !---------------
  ! allocate arrays
  !---------------
  ALLOCATE(pft(nlon,nlat,npft))
  allocate(vals_swap(npft))
  allocate(iord(npft))
  ALLOCATE(landmask(nlon,nlat))

  !----------------------
  ! read the PFT variable
  !----------------------
  WRITE(6,'(A)') '* reading pft data'

  CALL check(NF90_INQ_VARID(ncid, "PCT_PFT", ivarid))
  CALL check(NF90_GET_VAR(ncid, ivarid, pft ))
  landmask = sum(pft,3)
  where (landmask > 0.0)
   landmask = 1.0
  else where
   landmask = 0.0
  end where
  CALL check(NF90_CLOSE(ncid))

  print *,'writing low resolution data'
  !---------------------------------
  ! 7.1 open low-res clm netcdf file 
  !---------------------------------
  CALL check(NF90_OPEN(path=clm_lowres,mode=nf90_write,ncid=ncid))

  ! read lat and lon
  CALL check(NF90_INQ_DIMID(ncid, "lat", dimid))
  CALL check(NF90_INQUIRE_DIMENSION(ncid, dimid, len = nlat_clm))
  CALL check(NF90_INQ_DIMID(ncid, "lon", dimid))
  CALL check(NF90_INQUIRE_DIMENSION(Ncid, dimid, len = nlon_clm))

  ! read teh land mask
  ALLOCATE(landmask_clm(nlon_clm,nlat_clm))
  CALL check(NF90_INQ_VARID(ncid, "LANDMASK", iVarId))
  CALL check(NF90_GET_VAR(ncid, iVarId, landmask_clm ))

  ALLOCATE(pft_clm(nlon_clm,nlat_clm,npft))
  ALLOCATE(pft_clm_org(nlon_clm,nlat_clm,npft))
  CALL check(NF90_INQ_VARID(ncid, "PCT_PFT", iVarId))
  CALL check(NF90_GET_VAR(ncid, iVarId, pft_clm ))

  pft_clm_org = pft_clm

  ! aggregrate the PFT top coarse scale
  
  res_fac=nlon/nlon_clm
  DO ilat=1,nlat_clm
    ilat1=(ilat-1)*res_fac+1
    ilat2=ilat1+res_fac-1
    DO ilon=1,nlon_clm
      IF (landmask_clm(ilon,ilat)>0.0) THEN
        ilon1=(ilon-1)*res_fac+1
        ilon2=ilon1+res_fac-1
        do ipft = 1 , npft
          oldpft = pft_clm(ilon,ilat,ipft)
          pft_clm(ilon,ilat,ipft) = 0.0
          res_fac_m2 = 0.0
          do jj = ilat1 , ilat2
            do ii = ilon1 , ilon2
              if ( landmask(ii,jj) > 0.0 ) then
                res_fac_m2 = res_fac_m2 + 1.0
                pft_clm(ilon,ilat,ipft) = pft_clm(ilon,ilat,ipft) + pft(ii,jj,ipft)
              end if
            end do
          end do
          if ( res_fac_m2 > 0.0 ) then
            pft_clm(ilon,ilat,ipft) = nint(pft_clm(ilon,ilat,ipft)/res_fac_m2)
          else
            pft_clm(ilon,ilat,ipft) = oldpft
          end if
        end do
      ENDIF       
    ENDDO
  ENDDO

  DO ilat=1,nlat_clm
    DO ilon=1,nlon_clm
      pxerr = sum(pft_clm_org(ilon,ilat,:))
      do ipft = 1 , npft
        pxerr = pxerr - pft_clm(ilon,ilat,ipft)
      end do
      if ( abs(pxerr) > 0.0 ) then
        print *, 'Summing up to ',sum(pft_clm_org(ilon,ilat,:)),' in ',ilon,ilat,' to correct.'
        call sortpatch(pft_clm(ilon,ilat,:),vals_swap,iord)
        totpft = sum(vals_swap)
        totadj = 0.0
        do ipft = 1 , npft
          adjust = (vals_swap(ipft)/totpft)*pxerr
          if ( abs(adjust) > 0.0 ) then
            totadj = totadj + nint(adjust)
            pft_clm(ilon,ilat,iord(ipft)) = vals_swap(ipft) + nint(adjust)
          end if
        end do
        pxerr = pxerr - totadj
        if ( abs(pxerr) > 0.0 ) then
          pft_clm(ilon,ilat,iord(1)) = pft_clm(ilon,ilat,iord(1)) + pxerr
        end if
      end if
    END DO
  END DO

! overwrite the PFT variable
  CALL check(NF90_INQ_VARID(ncid, "PCT_PFT", iVarId))
  CALL check(NF90_PUT_VAR(ncid, ivarid, pft_clm) )
  CALL check(NF90_CLOSE(ncid))

!---------------------------------------------------------
CONTAINS

  SUBROUTINE check(status)
    USE netcdf
    IMPLICIT NONE
    INTEGER, INTENT(in) :: status
    IF (status /= nf90_noerr) THEN
      PRINT *,TRIM(NF90_STRERROR(status))
      STOP 'Bad NETCDF status'
    END IF
  END SUBROUTINE check

  recursive subroutine sortpatch(vals,svals,ird,lsub)
    implicit none
    real , dimension(:) , intent(in) :: vals
    real , dimension(:) , intent(inout) :: svals
    integer , dimension(:) , intent(inout) :: ird
    logical , optional :: lsub
    integer :: i , iswap
    real :: rswap
    if ( .not. present(lsub) ) then
      do i = 1 , size(vals)
        ird(i) = i
        svals(i) = vals(i)
      end do
    end if
    do i = 1 , size(vals)-1
      if ( svals(i) < svals(i+1) ) then
        rswap = svals(i+1)
        iswap = ird(i+1)
        svals(i+1) = svals(i)
        ird(i+1) = ird(i)
        svals(i) = rswap
        ird(i) = iswap
        call sortpatch(vals,svals,ird,.true.)
      end if
    end do
  end subroutine sortpatch

END
