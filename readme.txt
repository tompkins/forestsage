
************************************       
     Forest-Sage-Release v1.0      *
************************************

- What software I need to run the model ?

 * You need a fortran 90 compiler. We have tested this with GNU gfortran 4.X
   and Intel ifort 11.x-13.x

 * NetCDF Fortran library available from:

   http://www.unidata.ucar.edu/downloads/netcdf/ftp

 * FORTRAN GIS library which is available from:

  http://prdownload.berlios.de/fortrangis/files/fortrangis

- What data I need to run the model ?

 * Initial dataset can be found at:

 http://clima-dods.ictp.trieste.it/data/d10/FOREST_SAGE


- How do I run the model ?

  1. Compile the code

     $ make

  2. Launch the script :

    ./forest_sage_scenario.run YYYY yyyy [0,1]

    where :
        YYYY = start year of the simulation
        yyyy = end year of the simulation
        flag for restart

   Eg. Assuming that the CLM3.5 matrix refers at 2000, running a test run
       for 10 years without restart, it would be:
           ./forest_sage.run 2001 2010 0 

  3. To set the FS parameters change values in the input namelist file

- Where to ask questions about the model ?

For any question regarding the FS model please contact:

tompkins@ictp.it
caporaso@ictp.it
